const express = require('express');
const app = express();
const bodyParser = require("body-parser");
const rateLimit = require("express-rate-limit");
const nodemailer = require("nodemailer");
const port = 4999;
require("dotenv").config();

app.use(bodyParser.json());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/public'));

const emailLimiter = rateLimit({
    windowMs: 60 * 60 * 1000, // 1 hour window
    max: 3, // start blocking after 5 requests
    message:
        "Too many emails sent"
});

app.use("/mail", emailLimiter);

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.EMAIL_NAME,
        pass: process.env.EMAIL_PASSWORD
    }
});

app.get('/', (req, res) => {
    res.sendFile("./html/index.html", { root: __dirname });
});

app.post("/mail", (req, res) => {
    let email = req.body.email;
    let name = req.body.name;
    let message = req.body.message;

    const mailOptions = {
        from: process.env.EMAIL_NAME,
        to: "philip.hlm@live.se",
        subject: "Contact Me Form Portfolio",
        text: "" + name + " wrote: \n" + message + " \n " + email
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
            res.send("success");
        }
    });

});

app.listen(port);