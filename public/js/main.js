function sendMail() {
    let nameVal = document.getElementById("nameInput").value;
    let emailVal = document.getElementById("emailInput").value;
    let messageVal = document.getElementById("messageInput").value;

    $.post("/mail", { name: nameVal, email: emailVal, message: messageVal }, function (data) {
        if (data == "success") {
            alert("It has been sent");
        }
        else if (data == "Too many emails sent") {
            alert("You have sent too many emails, wait a while.");
        }
    });
}

